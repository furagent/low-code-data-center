package com.yabushan.form.service.impl;

import java.util.List;
import java.util.Map;

import com.yabushan.common.utils.DateUtils;
import com.yabushan.form.domain.JimuReport;
import com.yabushan.form.domain.JimuReportDb;
import com.yabushan.form.domain.JimuReportDbField;
import com.yabushan.form.service.IJimuReportDbFieldService;
import com.yabushan.form.service.IJimuReportDbService;
import com.yabushan.form.service.IJimuReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.yabushan.form.mapper.FormInfosMapper;
import com.yabushan.form.domain.FormInfos;
import com.yabushan.form.service.IFormInfosService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 自定义表单Service业务层处理
 *
 * @author yabushan
 * @date 2021-06-26
 */
@Service
public class FormInfosServiceImpl implements IFormInfosService
{
    @Autowired
    private FormInfosMapper formInfosMapper;
    @Autowired
    private IJimuReportDbFieldService fieldService;
    @Autowired
    private IJimuReportDbService dbService;
    @Autowired
    private IJimuReportService reportService;

    /**
     * 查询自定义表单
     *
     * @param id 自定义表单ID
     * @return 自定义表单
     */
    @Override
    public FormInfos selectFormInfosById(Long id)
    {
        return formInfosMapper.selectFormInfosById(id);
    }

    /**
     * 查询自定义表单列表
     *
     * @param formInfos 自定义表单
     * @return 自定义表单
     */
    @Override
    public List<FormInfos> selectFormInfosList(FormInfos formInfos)
    {
        return formInfosMapper.selectFormInfosList(formInfos);
    }

    /**
     * 新增自定义表单
     *
     * @param formInfos 自定义表单
     * @return 结果
     */
    @Override
    public int insertFormInfos(FormInfos formInfos)
    {
        formInfos.setCreateTime(DateUtils.getNowDate());
        return formInfosMapper.insertFormInfos(formInfos);
    }

    /**
     * 修改自定义表单
     *
     * @param formInfos 自定义表单
     * @return 结果
     */
    @Override
    public int updateFormInfos(FormInfos formInfos)
    {
        formInfos.setUpdateTime(DateUtils.getNowDate());
        return formInfosMapper.updateFormInfos(formInfos);
    }

    /**
     * 批量删除自定义表单
     *
     * @param ids 需要删除的自定义表单ID
     * @return 结果
     */
    @Override
    public int deleteFormInfosByIds(Long[] ids)
    {
        return formInfosMapper.deleteFormInfosByIds(ids);
    }

    /**
     * 删除自定义表单信息
     *
     * @param id 自定义表单ID
     * @return 结果
     */
    @Override
    public int deleteFormInfosById(Long id)
    {
        return formInfosMapper.deleteFormInfosById(id);
    }

    @Override
    @Transactional
    public void createTable(String sql, FormInfos formInfos, JimuReport report, JimuReportDb reportDb, List<JimuReportDbField> fields) {
        //生成表
        formInfosMapper.dynamicsCreate(sql);
        //更新表单状态
        formInfosMapper.updateFormInfos(formInfos);
        //插入报表
        reportService.insertJimuReport(report);
        //插入报表API
        dbService.insertJimuReportDb(reportDb);
        //插入报表字段
        for(int i=0;i<fields.size();i++){
            fieldService.insertJimuReportDbField(fields.get(i));
        }
    }

    @Override
    public void dynamicsInsert(String sql) {
        formInfosMapper.dynamicsInsert(sql);
    }

    @Override
    public void dynamicsUpdate(String sql) {
        formInfosMapper.dynamicsUpdate(sql);
    }

    @Override
    public List<Map> dynamicsSelect(String sql) {
        return formInfosMapper.dynamicsSelect(sql);
    }

    @Override
    public void dynamicsDelete(String sql) {
        formInfosMapper.dynamicsDelete(sql);
    }
}
