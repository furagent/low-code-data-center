package com.yabushan.form.mapper;

import java.util.List;
import java.util.Map;

import com.yabushan.form.domain.FormInfos;
import org.apache.ibatis.annotations.Param;

/**
 * 自定义表单Mapper接口
 *
 * @author yabushan
 * @date 2021-06-26
 */
public interface FormInfosMapper
{
    /**
     * 查询自定义表单
     *
     * @param id 自定义表单ID
     * @return 自定义表单
     */
    public FormInfos selectFormInfosById(Long id);

    /**
     * 查询自定义表单列表
     *
     * @param formInfos 自定义表单
     * @return 自定义表单集合
     */
    public List<FormInfos> selectFormInfosList(FormInfos formInfos);

    /**
     * 新增自定义表单
     *
     * @param formInfos 自定义表单
     * @return 结果
     */
    public int insertFormInfos(FormInfos formInfos);

    /**
     * 修改自定义表单
     *
     * @param formInfos 自定义表单
     * @return 结果
     */
    public int updateFormInfos(FormInfos formInfos);

    /**
     * 删除自定义表单
     *
     * @param id 自定义表单ID
     * @return 结果
     */
    public int deleteFormInfosById(Long id);

    /**
     * 批量删除自定义表单
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFormInfosByIds(Long[] ids);

    //生成数据库表
    void dynamicsCreate(@Param("paramSQL") String sql);

    void dynamicsInsert(@Param("paramSQL") String sql);

    void dynamicsUpdate(@Param("paramSQL") String sql);

    void dynamicsDelete(@Param("paramSQL") String sql);

    List<Map> dynamicsSelect(@Param("paramSQL") String sql);
}
